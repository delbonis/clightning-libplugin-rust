extern crate bindgen;

use std::env;
use std::path::PathBuf;
use std::process::Command;

fn main() {
    // Borrowed partly from http://fitzgeraldnick.com/2016/12/14/using-libbindgen-in-build-rs.html#step-1

    // Run the configure script...
    Command::new("./configure")
        .args(&["--enable-static"])
        .current_dir(PathBuf::from("./lightning"))
        .status()
        .expect("build script failed!");

    // Figure out the cflags so we can pass to clang.
    let cl_cflags = make_wrapper_dump("cflags");
    let cflags_parts: Vec<String> = cl_cflags.split_whitespace().map(String::from).collect();

    // Generate the bindings.  The dir stuff is because it's simpler for us.
    let cd = env::current_dir().unwrap();
    env::set_current_dir(PathBuf::from("./lightning")).unwrap();
    let bindings = bindgen::Builder::default()
        .header("../wrapper.h")
        .clang_args(cflags_parts)
        // These break things.
        .blacklist_type("u8")
        .blacklist_type("u16")
        .blacklist_type("u32")
        .blacklist_type("u64")
        .whitelist_function("plugin_.+")
        .whitelist_function("command_.+")
        .whitelist_function("rpc_.+")
        .whitelist_function("forward_[(error)(result)]")
        .whitelist_type("command.*")
        .whitelist_type("param_cbx")
        .whitelist_function("json_.+") // json
        .whitelist_function("param") // json
        .whitelist_function("jsmn.*") // json
        .whitelist_type("jsmn.*") // json
        .whitelist_function("send_outreq_") // idk
        .whitelist_function(".*_eq") // structeq
        .whitelist_type("preimage") // bitcoin
        .whitelist_type("amount_.*")
        .whitelist_type("pubkey")
        .whitelist_type("short_channel_id")
        .whitelist_var("JSONRPC2_.+")
        .whitelist_var("PAY_.+")
        .whitelist_var("FUND(ING)?_.+")
        .whitelist_var("INVOICE_.+")
        .whitelist_var("deprecated_apis")
        .derive_debug(true)
        .derive_partialeq(true)
        .rustfmt_bindings(true)
        .generate()
        .expect("Unable to generate libplugin bindings");
    env::set_current_dir(cd).unwrap();

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}

fn make_wrapper_dump(v: &str) -> String {
    let out = Command::new("make")
        .current_dir(PathBuf::from("./lightning"))
        .arg("-f")
        .arg("../Makefile.wrapper")
        .arg(format!("dump-{}", v))
        .output()
        .expect("unable to invoke make");

    String::from_utf8(out.stdout).expect("make output bad format")
}
