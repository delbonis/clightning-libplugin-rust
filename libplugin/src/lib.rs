pub extern crate cl_libplugin_sys as lpsys;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Error {
    FundingBroadcastFail,
    FundCannotAfford,
    FundMaxExceeded,
    FundOutputIsDust,
    InvoiceLabelExists,
    InvoicePreimageExists,
    RpcInvalidRequest,
    RpcMethodNotFound,
    RpcInvalidParams,
    PayDestPermFail,
    PayInvoiceExpired,
    PayInProgress,
    PayNoSuchPayment,
    PayRHashUsed,
    PayRouteNotFound,
    PayRouteTooExpensive,
    PayStoppedRetrying,
    PayTryOtherRoute,
    PayUnparseableOnion,
    PayUnspecifiedError,
    Unknown(i32),
}

impl Error {
    pub fn from_raw(v: i32) -> Error {
        use Error::*;
        // Weird gross type conversions because that's what bindgen gave us.
        match v as u32 {
            lpsys::FUNDING_BROADCAST_FAIL => FundingBroadcastFail,
            lpsys::FUND_CANNOT_AFFORD => FundCannotAfford,
            lpsys::FUND_MAX_EXCEEDED => FundMaxExceeded,
            lpsys::FUND_OUTPUT_IS_DUST => FundOutputIsDust,
            lpsys::INVOICE_LABEL_ALREADY_EXISTS => InvoiceLabelExists,
            lpsys::INVOICE_PREIMAGE_ALREADY_EXISTS => InvoicePreimageExists,
            lpsys::PAY_DESTINATION_PERM_FAIL => PayDestPermFail,
            lpsys::PAY_INVOICE_EXPIRED => PayInvoiceExpired,
            lpsys::PAY_IN_PROGRESS => PayInProgress,
            lpsys::PAY_NO_SUCH_PAYMENT => PayNoSuchPayment,
            lpsys::PAY_RHASH_ALREADY_USED => PayRHashUsed,
            lpsys::PAY_ROUTE_NOT_FOUND => PayRouteNotFound,
            lpsys::PAY_ROUTE_TOO_EXPENSIVE => PayRouteTooExpensive,
            lpsys::PAY_STOPPED_RETRYING => PayStoppedRetrying,
            lpsys::PAY_TRY_OTHER_ROUTE => PayTryOtherRoute,
            lpsys::PAY_UNPARSEABLE_ONION => PayUnparseableOnion,
            lpsys::PAY_UNSPECIFIED_ERROR => PayUnspecifiedError,
            _ => match v as i32 {
                lpsys::JSONRPC2_INVALID_REQUEST => RpcInvalidRequest,
                lpsys::JSONRPC2_INVALID_PARAMS => RpcInvalidParams,
                _ => Unknown(v),
            },
        }
    }
}
